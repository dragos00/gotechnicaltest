# Junior Go Developer Technical Test
### Project Structure
I created two packages, **dataprocessing** and **serverapi** to separate responsibility between the server side and data processing. Package **dataprocessing** contains three functions responsible for searching and filtering specific data from the input. Package **serverapi** contains all functions related to the server. **Start** function is responsible for starting the server on the specified port. Both packages contain test files.

### Data Store
Given this scenario where user permission consists of key value pairs where the key is a string and the permission can be boolean, string or numerical, I chose a map for data store. More specific, I chose a ```map[string]interface{}``` for multiple reasons. Map provides fast retrieval, search, insert and delete operations. I chose map value of type ```interface{}``` because permission values can be boolean, numerical or string. The map is populated locally when the service starts.

### Implementation
Package **dataprocessing** contains three exported functions used when handling requests in **serverapi** package: one for retrieving collection of permissions, one for retrieving permissions for the named service and one for retrieving a list of users with specified service, feature and permission. They take data in the form of `map[string]interface{}` and specified username, service etc. then search them in the map. The only case when error is not nil occurs when the specified placeholder is not found. Another implementation detail worth mentioning here is the function `isEmpty`. It takes an `interface{}` and returns true if it's empty and false otherwise. Because the value of permissions can be boolean, numeric or string and these types have different nil values I made a `switch case`. If the type passed as `interface{}` is numeric then I check if the value is 0 and if the type is string I check if the value is "".

I thought of a way to inject the data to be used in handlers in **serverapi** package. I created a structure called `DataHandler` which contains a field data of type `map[string]interface{}`. Then I created a constructor `NewDataHandler` which takes that map as a parameter and returns a `DataHandler` structure containing the map. I defined two methods that has a receiver of type `DataHandler` named `dh`. This way I can inject any data, no matter where it comes from, as long as the data type is `map[string]interface{}`. This choice also favors testing the functionality of these methods because I can easily inject any test data and see what is the output.

The first method, `returnUserPermissions` is responsible for handling two endpoints: **/v1/user/:user** and **/v1/user/:user/:service**. To solve this, I created another function called ``returnPlaceHolders`` which takes the url in string form, validates and returns the placeholders in a string slice. It returns an error if any other characters are inserted or if too many placeholders are inserted. Based on the output of this function I made a ``switch case``. If the length of the output is one, then only the user was inserted and the function responsible for retrieving user permission is called. If the length of the output is two then user and service was inserted and the function responsible for retrieving user permission for this service is called. If the error is not nil then ``http.Error`` is called with status code 400.

The second method, `returnUserListWithCertainPermission` is responsible for handling the last endpoint: `/v1/service/:service/:feature/:permission`. The same function `returnPlaceHolders` is called. If the error is nil the function responsible for retrieving a list of users with specified service, feature and permission is called.

The only request method allowed is GET(checked for both methods mentioned). In both cases I manage specific status codes and call `http.Error` if necessary. If everything works fine then the output is encoded in json format and status code will be 200 OK. In other cases I call `http.Error` with 403 code (only get request allowed), code 400 (invalid placeholders or error encountered at encoding part of the response) and code 404 (searched data not found).

Both packages contain files for testing. For tests related to http handlers I used the base Go package `httptest`. Using this package I can make a request with `httptest.NewRequest` which is a wrapper around `http.NewRequest` so I don't have to check the error making a request. Then I create a `httptest.NewRecorder` which makes a recorder that the HTTP handler writes to as its `http.ResponseWriter`, and it captures all the changes that would have been returned to a client caller. Using this, there’s no need to start a server. I just hand the recorder directly to the function, and it invokes it the same way it would if the request came in over HTTP.

### How to use this service
After you clone the repository open the project. In main, you can define a map with your desired data. Create a new data handler with `serverapi.NewDataHandler` and pass it to `serverapi.Start` function along with your desired port address for your server.




