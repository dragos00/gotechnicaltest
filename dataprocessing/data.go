package dataprocessing

import (
	"errors"
	"strings"
)

// GetUserPermissions takes a map and a username and returns a map
// containing all user permissions. It returns an error if username
// is not found otherwise error is nil
func GetUserPermissions(data map[string]interface{}, username string) (map[string]interface{}, error) {
	userPermission := make(map[string]interface{})
	for key, value := range data {
		if strings.Contains(key, username) && !isEmpty(value) {
			userPermission[key] = value
		}
	}
	if len(userPermission) == 0 {
		return nil, errors.New("username not found")
	}
	return userPermission, nil
}

// GetUserPermissionsForNamedService takes a map, a username, and a service
// and returns a map containing all user permissions for the named service
// It returns an error if the username or service is not found otherwise
// error is nil
func GetUserPermissionsForNamedService(data map[string]interface{}, username string, service string) (map[string]interface{}, error) {
	userPermission := make(map[string]interface{})
	for key, value := range data {
		if strings.Contains(key, username) && strings.Contains(key, service) && !isEmpty(value) {
			userPermission[key] = value
		}
	}
	if len(userPermission) == 0 {
		return nil, errors.New("username or service not found")
	}
	return userPermission, nil
}

// GetUserListWithCertainPermission takes a map and a string slice
// containing service, feature and permission and returns a map with
// all users with specified permission. It returns an error if service,
// feature or permission is not found otherwise error is nil
func GetUserListWithCertainPermission(data map[string]interface{}, placeHolders []string) ([]string, error) {
	var userList []string
	for key, value := range data {
		if strings.Contains(key, placeHolders[0]) && strings.Contains(key, placeHolders[1]) && strings.Contains(key, placeHolders[2]) {
			if !isEmpty(value) {
				stringParsed := strings.Split(key, ".")
				username := stringParsed[2]
				userList = append(userList, username)
			}
		}
	}
	if len(userList) == 0 {
		return nil, errors.New("service, feature or permission not found")
	}
	return userList, nil
}

func isEmpty(value interface{}) bool {
	switch value.(type) {
	case string:
		if value == "" {
			return true
		}
	case int:
		if value == 0 {
			return true
		}
	}
	return false
}
