package dataprocessing

import (
	"reflect"
	"testing"
)

var testData = map[string]interface{}{
	"payments.accounts.andrew.create":     true,
	"transactions.accounts.andrew.create": true,
	"payments.accounts.andrew.modify":     "files only",
	"payments.accounts.ana.create":        "files only",
	"payments.accounts.ana.modify":        false,
	"payments.accounts.mathew.create":     1792,
	"payments.accounts.mathew.modify":     1149,
}

var wantData = map[string]interface{}{
	"payments.accounts.andrew.create":     true,
	"payments.accounts.andrew.modify":     "files only",
	"transactions.accounts.andrew.create": true,
}

func TestGetUserPermissionsNoError(t *testing.T) {
	got, _ := GetUserPermissions(testData, "andrew")
	if !reflect.DeepEqual(got, wantData) {
		t.Errorf("got %v, want %v", got, wantData)
	}
}

func TestGetUserPermissionsError(t *testing.T) {
	_, err := GetUserPermissions(testData, "alex")
	if err == nil {
		t.Errorf("should return not found error")
	}
}

func TestGetUserPermissionsNumericInput(t *testing.T) {
	_, err := GetUserPermissions(testData, "1292")
	if err == nil {
		t.Errorf("should return error")
	}
}

func TestGetUserPermissionsNoInput(t *testing.T) {
	var testData1 map[string]interface{}
	userPermissions, _ := GetUserPermissions(testData1, "andrew")
	if userPermissions != nil {
		t.Errorf("user permissions should be nil")
	}
}

func TestGetUserListWithCertainPermissionError(t *testing.T) {
	placeHolders := make([]string, 3, 3)
	placeHolders[0] = "some string"
	_, err := GetUserListWithCertainPermission(testData, placeHolders)
	if err == nil {
		t.Errorf("should return error")
	}
	placeHolders[2] = "delete"
	_, err = GetUserListWithCertainPermission(testData, placeHolders)
	if err == nil {
		t.Errorf("should return error")
	}
}

func TestGetUserPermissionsForNamedService(t *testing.T) {
	username := "andrew"
	service := "transactions"
	got, _ := GetUserPermissionsForNamedService(testData, username, service)
	want := make(map[string]interface{})
	want["transactions.accounts.andrew.create"] = true
	if !reflect.DeepEqual(got, want) {
		t.Errorf("got %v, want %v", got, want)
	}
}

func TestGetUserPermissionsForNamedServiceInvalidInput(t *testing.T) {
	username := ""
	service := ""
	_, err := GetUserPermissionsForNamedService(nil, username, service)
	if err == nil {
		t.Errorf("should return error")
	}
}

func TestIsEmpty(t *testing.T) {
	var value interface{}
	value = 0
	if isEmpty(value) == false {
		t.Errorf("should return true")
	}
	value = ""
	if isEmpty(value) == false {
		t.Errorf("should return true")
	}
	value = 1234
	if isEmpty(value) == true {
		t.Errorf("should return false")
	}
	value = "value"
	if isEmpty(value) == true {
		t.Errorf("should return false")
	}
}
