package main

import "GOTest/serverapi"

func main() {
	dataMap := make(map[string]interface{})
	dataMap["payments.accounts.andrew.create"] = true
	dataMap["payments.accounts.andrew.modify"] = "files only"
	dataMap["payments.accounts.ana.create"] = 1972
	dataMap["payments.accounts.ana.modify"] = true
	dataMap["payments.accounts.ana.delete"] = "files only"
	dataMap["payments.resource.andrew.create"] = 1192
	dataMap["payments.resource.john.delete"] = 1192
	dataMap["payments.resource.john.modify"] = 1192
	dataMap["transactions.accounts.ana.modify"] = true
	dataMap["payments.accounts.john.create"] = 1192
	dataMap["transactions.accounts.ana.modify"] = false
	dataMap["transactions.accounts.ana.modify"] = true
	dataMap["transactions.accounts.andrew.modify"] = 0
	dataMap["transactions.accounts.john.modify"] = 0
	dataMap["transactions.accounts.andrew.delete"] = ""
	dataMap["transactions.accounts.john.delete"] = ""

	dh := serverapi.NewDataHandler(dataMap)
	serverapi.Start(":8081", dh)

}
