package serverapi

import (
	"GOTest/dataprocessing"
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"strings"
	"unicode"
)

// Start function starts the server on the port indicated by address string
// It is using a DataHandler to inject data and handle requests based on it
func Start(address string, handler DataHandler) {
	handleRequests(handler)
	log.Print(http.ListenAndServe(address, nil))
}

func handleRequests(handler DataHandler) {
	http.HandleFunc("/v1/user/", handler.returnUserPermissions)
	http.HandleFunc("/v1/service/", handler.returnUserListWithCertainPermission)
}

func NewDataHandler(d map[string]interface{}) DataHandler {
	return DataHandler{d}
}

type DataHandler struct {
	data map[string]interface{}
}

func (dh DataHandler) returnUserPermissions(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only GET request allowed", 403)
		log.Print(errors.New("not a GET request"))
		return
	}
	url := r.URL.String()
	placeHolders, err := returnPlaceHolders(url)
	if err != nil {
		http.Error(w, "invalid parameters", 400)
		log.Print(err.Error())
		return
	}
	switch len(placeHolders) {
	case 1:
		{
			username := placeHolders[0]
			userPermissions, err := dataprocessing.GetUserPermissions(dh.data, username)
			if err != nil {
				http.Error(w, "user name not found", 404)
				log.Print(err.Error())
				return
			}
			w.Header().Set("Content-Type", "application/json")
			err = json.NewEncoder(w).Encode(userPermissions)
			if err != nil {
				http.Error(w, "body parse error", 400)
				log.Print(err.Error())
				return
			}
		}
	case 2:
		{
			username := placeHolders[0]
			service := placeHolders[1]
			userPermissions, err := dataprocessing.GetUserPermissionsForNamedService(dh.data, username, service)
			if err != nil {
				http.Error(w, "user name or service not found", 404)
				log.Print(err.Error())
				return
			}
			w.Header().Set("Content-Type", "application/json")
			err = json.NewEncoder(w).Encode(userPermissions)
			if err != nil {
				http.Error(w, "body parse error", 400)
				log.Print(err.Error())
				return
			}
		}
	default:
		http.Error(w, "invalid parameters", 400)
	}
}

func (dh DataHandler) returnUserListWithCertainPermission(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only GET request allowed", 403)
		log.Print(errors.New("not a GET request"))
		return
	}
	url := r.URL.String()
	placeHolders, err := returnPlaceHolders(url)
	if err != nil {
		http.Error(w, "invalid parameters", 400)
		log.Print(err.Error())
		return
	}
	userList, err := dataprocessing.GetUserListWithCertainPermission(dh.data, placeHolders)
	if err != nil {
		http.Error(w, "permission not found", 404)
		log.Print(err.Error())
		return
	}
	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(userList)
	if err != nil {
		http.Error(w, "body parse error", 400)
		log.Print(err.Error())
		return
	}
}

func returnPlaceHolders(url string) ([]string, error) {
	elements := strings.Split(url, "/:")
	if len(elements) == 1 {
		return nil, errors.New("invalid input")
	}
	var parameters []string
	for i := 1; i <= len(elements)-1; i++ {
		if !isValidString(elements[i]) {
			return nil, errors.New("invalid input")
		}
		parameters = append(parameters, elements[i])
	}
	if len(parameters) >= 4 {
		return nil, errors.New("invalid input")
	}
	return parameters, nil
}

func isValidString(s string) bool {
	if s == "" {
		return false
	}
	for _, r := range s {
		if !unicode.IsLetter(r) {
			return false
		}
	}
	return true
}
