package serverapi

import (
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"
)

var testData = map[string]interface{}{
	"payments.accounts.ana.create": true,
	"payments.accounts.ana.modify": true,
}

func TestReturnUserPermissionsStatusOk(t *testing.T) {
	req := httptest.NewRequest(http.MethodGet, "http://localhost:8081/v1/user/:ana", nil)
	w := httptest.NewRecorder()
	handler := NewDataHandler(testData)
	handler.returnUserPermissions(w, req)
	res := w.Result()
	if res.StatusCode != 200 {
		t.Errorf("expected status code 200 got %v", res.StatusCode)
	}
}

func TestReturnUserPermissionsForServiceStatusOk(t *testing.T) {
	req := httptest.NewRequest(http.MethodGet, "http://localhost:8081/v1/user/:ana/:payments", nil)
	w := httptest.NewRecorder()
	handler := NewDataHandler(testData)
	handler.returnUserPermissions(w, req)
	res := w.Result()
	if res.StatusCode != 200 {
		t.Errorf("expected status code 200 got %v", res.StatusCode)
	}
}

func TestReturnUserPermissionsInvalidParameters(t *testing.T) {
	req := httptest.NewRequest(http.MethodGet, "http://localhost:8081/v1/user/:1234", nil)
	w := httptest.NewRecorder()
	handler := NewDataHandler(testData)
	handler.returnUserPermissions(w, req)
	res := w.Result()
	if res.StatusCode != 400 {
		t.Errorf("expected status code 400 got %v", res.StatusCode)
	}
}

func TestReturnUserPermissionsInvalidParametersForService(t *testing.T) {
	req := httptest.NewRequest(http.MethodGet, "http://localhost:8081/v1/user/:ana/:1234", nil)
	w := httptest.NewRecorder()
	handler := NewDataHandler(testData)
	handler.returnUserPermissions(w, req)
	res := w.Result()
	if res.StatusCode != 400 {
		t.Errorf("expected status code 400 got %v", res.StatusCode)
	}
}

func TestReturnUserPermissionsNoData(t *testing.T) {
	req := httptest.NewRequest(http.MethodGet, "http://localhost:8081/v1/user", nil)
	w := httptest.NewRecorder()
	handler := NewDataHandler(testData)
	handler.returnUserPermissions(w, req)
	res := w.Result()
	if res.StatusCode != 400 {
		t.Errorf("expected status code 400 got %v", res.StatusCode)
	}
}

func TestReturnUserPermissionsNotFound(t *testing.T) {
	req := httptest.NewRequest(http.MethodGet, "http://localhost:8081/v1/user/:andrew", nil)
	w := httptest.NewRecorder()
	handler := NewDataHandler(testData)
	handler.returnUserPermissions(w, req)
	res := w.Result()
	if res.StatusCode != 404 {
		t.Errorf("expected status code 404 got %v", res.StatusCode)
	}
}

func TestReturnUserPermissionsServiceNotFound(t *testing.T) {
	req := httptest.NewRequest(http.MethodGet, "http://localhost:8081/v1/user/:ana/:transactions", nil)
	w := httptest.NewRecorder()
	handler := NewDataHandler(testData)
	handler.returnUserPermissions(w, req)
	res := w.Result()
	if res.StatusCode != 404 {
		t.Errorf("expected status code 404 got %v", res.StatusCode)
	}
}

func TestReturnUserListWithCertainPermissionStatusOk(t *testing.T) {
	req := httptest.NewRequest(http.MethodGet, "http://localhost:8081/v1/service/:payments/:accounts/:create", nil)
	w := httptest.NewRecorder()
	handler := NewDataHandler(testData)
	handler.returnUserListWithCertainPermission(w, req)
	res := w.Result()
	if res.StatusCode != 200 {
		t.Errorf("expected status code 200 got %v", res.StatusCode)
	}
}

func TestReturnUserListWithCertainPermissionInvalidParameters(t *testing.T) {
	req := httptest.NewRequest(http.MethodGet, "http://localhost:8081/v1/service/:payments/:accounts/:1234", nil)
	w := httptest.NewRecorder()
	handler := NewDataHandler(testData)
	handler.returnUserListWithCertainPermission(w, req)
	res := w.Result()
	if res.StatusCode != 400 {
		t.Errorf("expected status code 400 got %v", res.StatusCode)
	}
}

func TestReturnUserListWithCertainPermissionNotFound(t *testing.T) {
	req := httptest.NewRequest(http.MethodGet, "http://localhost:8081/v1/service/:payments/:accounts/:delete", nil)
	w := httptest.NewRecorder()
	handler := NewDataHandler(testData)
	handler.returnUserListWithCertainPermission(w, req)
	res := w.Result()
	if res.StatusCode != 404 {
		t.Errorf("expected status code 404 got %v", res.StatusCode)
	}
}

func TestReturnUserListWithCertainPermissionNoInput(t *testing.T) {
	req := httptest.NewRequest(http.MethodGet, "http://localhost:8081/v1/service", nil)
	w := httptest.NewRecorder()
	handler := NewDataHandler(testData)
	handler.returnUserListWithCertainPermission(w, req)
	res := w.Result()
	if res.StatusCode != 400 {
		t.Errorf("expected status code 400 got %v", res.StatusCode)
	}
}

func TestReturnUserListWithCertainPermission(t *testing.T) {
	req := httptest.NewRequest(http.MethodGet, "http://localhost:8081/v1/service", nil)
	w := httptest.NewRecorder()
	handler := NewDataHandler(testData)
	handler.returnUserListWithCertainPermission(w, req)
	res := w.Result()
	if res.StatusCode != 400 {
		t.Errorf("expected status code 400 got %v", res.StatusCode)
	}
}

func TestReturnPlaceHolders(t *testing.T) {
	url := "http://localhost:8081/v1/service/:payments/:accounts/:modify"
	want := make([]string, 3, 3)
	want[0] = "payments"
	want[1] = "accounts"
	want[2] = "modify"
	got, _ := returnPlaceHolders(url)
	if !reflect.DeepEqual(got, want) {
		t.Errorf("got %v, want %v", got, want)
	}
}

func TestReturnPlaceHoldersError(t *testing.T) {
	badUrl1 := "http://localhost:8081/v1/service/:1230194"
	_, err := returnPlaceHolders(badUrl1)
	if err == nil {
		t.Errorf("should return invalid input error")
	}
	badUrl2 := "http://localhost:8081/v1/service/:payments/:accounts/:modify/:username"
	_, err = returnPlaceHolders(badUrl2)
	if err == nil {
		t.Errorf("should return invalid input error")
	}
	emptyUrl := ""
	_, err = returnPlaceHolders(emptyUrl)
	if err == nil {
		t.Errorf("should return invalid input error")
	}
}

func TestReturnPlaceHoldersNoError(t *testing.T) {
	url := "http://localhost:8081/v1/service/:payments/:accounts/:modify"
	_, err := returnPlaceHolders(url)
	if err != nil {
		t.Errorf("error %v", err)
	}
}

func TestIsValidStringFalse(t *testing.T) {
	s := ""
	if isValidString(s) {
		t.Errorf("should return false")
	}
}

func TestIsValidStringSymbolsInput(t *testing.T) {
	s := "<$%>?<>"
	if isValidString(s) {
		t.Errorf("should return false")
	}
}

func TestIsValidStringTrue(t *testing.T) {
	s := "valid"
	if !isValidString(s) {
		t.Errorf("should return true")
	}
}
